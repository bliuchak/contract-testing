const { v4: uuidv4 } = require('uuid');

const express = require('express')
const app = express()
const port = 3000

let users = {}

app.use(express.json());

app.get('/users', function(req, res) {
  res.status(200).json(users).send()
})

app.get('/user/:id', function(req, res) {
  userId = req.params.id
  user = users[userId]
  if (user) {
    res.status(200).json(user).send()
  } else {
    res.status(404).send()
  }
})

app.post('/user', function(req, res) {
  const userId = uuidv4()

  if (!req.body.firstname || !req.body.lastname) {
    res.status(400).send()
    return
  }

  users[userId] = {
    id: userId,
    firstname: req.body.firstname,
    lastname: req.body.lastname
  }

  res.status(200).send()
})

app.delete('/user/:id', function (req, res) {
  userId = req.params.id
  delete users[userId]
  res.status(204).send()
})

app.listen(port, function() {
  console.log(`Example app listening at http://localhost:${port}`)
})
